package co.simplon.apirest.repository;

import org.springframework.data.repository.CrudRepository;

import co.simplon.apirest.entity.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {


}
