package co.simplon.apirest.controller;


import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.apirest.entity.Person;
import co.simplon.apirest.repository.PersonRepository;

@RestController
@RequestMapping("/person")
public class FirstController {
	
	@Autowired
	private PersonRepository repo;

	@GetMapping("/first")
	public String first() {
		return "coucou";
	}
	@GetMapping("/{id}")
	public Optional<Person> find(@PathVariable("id") Integer id) {
		return repo.findById(id);
	}
	@PostMapping
	public ResponseEntity<Person> add(@RequestBody @Valid Person person) {
		repo.save(person);
		return ResponseEntity.status(201).body(person);
	}
	
	@GetMapping
	public Iterable<Person> findAll() {
		return repo.findAll();
	}
	
	
}
